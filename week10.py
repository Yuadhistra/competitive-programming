from collections import defaultdict
class Solution:
    def findItinerary(self, tickets):

        def dfs(src):
            arr = adjlist[src]
            while arr:
                dfs(arr.pop())
            res.append(src)
        
        res = []
        adjlist = defaultdict(list)
        tickets.sort()

        for src, dest in tickets:
            adjlist[src].append(dest)
        dfs('JFK')
        return res[::-1]


Ticket=[["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
g=Solution()
print(g.findItinerary(Ticket))       
